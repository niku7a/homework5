fun main() {

    val s = sayHello("Jemali")
    println(s)
    sayHello()

}

fun sayHello(name: String = "guliko"): String {
    return "Hello $name!"
}